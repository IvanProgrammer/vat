<?php

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}

/**
 * This function is used to add a card with a message in the box
 * @param string $gift_box_message = $_REQUEST[gift_box_message]
 * @param return $is_status true and false.
 * True if successfully added, if it false then an error will be issued an appropriate notice
 */
function fn_create_gift_box_message($gift_box_message, &$cart)
{

    $is_status = false;

    if (!empty($gift_box_message)) {
        $is_status = true;
        $cart['gift_box_message'] = $gift_box_message;
    }

    return $is_status;

}

/**
 * This function is used to add a card with a vat number
 * @param string $vat = $_REQUEST['VAT']
 */
function fn_create_vat_number($vat, &$cart)
{

    if (!empty($vat)) {
        $cart['VAT'] = $vat;
    }

}

/**
 * This function is used to delete a card with a vat number
 * @param string $vat = $_REQUEST['VAT']
 */
function fn_delete_vat_number(&$cart)
{

    $cart['VAT'] = '';

}


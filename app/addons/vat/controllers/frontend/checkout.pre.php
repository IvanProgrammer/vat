<?php
/***************************************************************************
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}

$cart = &Tygh::$app['session']['cart'];

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if ($mode == 'update_steps') {
        if (!empty($_REQUEST['gift_box_message'])) {
            $gift_box_message = $_REQUEST['gift_box_message'];
            $status = fn_create_gift_box_message($gift_box_message, $cart);

            if ($status) {
                fn_set_notification('N', __('vat.gift_box_message_status_ok'), $cart['gift_box_message']);
            }
        }

        if ($cart['user_data']['b_country'] == GREAT_BRITAIN) {
            if (!empty($_REQUEST['VAT'])) {
                $vat = $_REQUEST['VAT'];

                fn_create_vat_number($vat, $cart);
            }
        }
    }
    if ($mode == 'place_order') {
        if ($cart['user_data']['b_country'] != GREAT_BRITAIN) {

            fn_delete_vat_number($cart);
        }
    }

    return true;

}

<?php
/***************************************************************************
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/


namespace Tygh\Addons\Vat\Documents\Order;

use Tygh\Template\Document\Order\Context;
use Tygh\Template\IVariable;

/**
 * Class VatVariable
 * @package Tygh\Addons\Barcode\Documents\Order
 */
class VatVariable implements IVariable
{
    public $vat_number,$gift_box_message;

    public function __construct(Context $context)
    {
        $order = $context->getOrder();
        $VAT = $order->data['VAT'];

        $gift_box_message = $order->data['gift_box_message'];
        if(!empty($VAT)){
        	$this->vat_number = __('vat.number_vat')." : ".$VAT;
        }
        if(!empty($gift_box_message)){
        	$this->gift_box_message = __('vat.gift_box_message')." : ".$gift_box_message;
    	}
    }
}
{if $order_info.gift_box_message}
	<tr class="ty-orders-summary__row">
	    <td>{__("vat.gift_box_message")}:</td>
	    <td style="width: 57%" data-ct-orders-summary="summary-payment">
	        {$order_info.gift_box_message}
	    </td>
	</tr>
{/if}
{if $order_info.VAT}
	<tr class="ty-orders-summary__row">
	    <td>{__("vat.number_vat")}:</td>
	    <td style="width: 57%" data-ct-orders-summary="summary-payment">
	        {$order_info.VAT}
	    </td>
	</tr>
{/if}

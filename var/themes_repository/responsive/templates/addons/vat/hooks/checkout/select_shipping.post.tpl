<div class="ty-control-group">
	<label for="gift_box_message" class="ty-control-group__title">{__("vat.gift_box_message")}</label>
	<textarea id="gift_box_message" name="gift_box_message" class="cm-focus" rows="5" cols="72">{$cart['gift_box_message']}</textarea>
</div>
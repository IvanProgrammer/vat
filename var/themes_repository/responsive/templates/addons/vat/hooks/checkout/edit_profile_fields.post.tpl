<div class="clearfix js-country-test" {if $user_data['b_country'] != $smarty.const.GREAT_BRITAIN} style="display:none" {/if} data-ct-address="billing-address">
    <div class="checkout__block">
	    <div class="ty-control-group">
	        <label for="{$id_prefix}elm_vat" class="js-required-test {if $user_data['b_country'] == $smarty.const.GREAT_BRITAIN}cm-required{/if}">{__("vat.number_vat")}</label>
	        <input type="text" id="elm_vat" name="VAT" size="32" value="{$cart.VAT}" class="ty-input-text"/>
	    </div>
    </div>
</div>


{include file="common/subheader.tpl" title=__("vat.vat") target="#vat"}
<div class="collapse in" id="vat">
    <div class="control-group">
        <label class="control-label" for="elm_vat">{__("vat.gift_box_message")}:</label>
        <div class="controls">
			{$order_info['gift_box_message']}
        </div>
    </div>
    {if $order_info['VAT']}
    <div class="control-group">
        <label class="control-label" for="elm_vat">{__("vat.number_vat")}:</label>
        <div class="controls">
        	{$order_info['VAT']}
        </div>
    </div>
    {/if}
</div>

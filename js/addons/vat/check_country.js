(function (_, $) {
    $.ceEvent('on', 'ce.commoninit', function () {
        $('#elm_26').change(function () {
            if ($(this).val() != "GB") {
                $('.js-country-test').css('display', 'none');
                $('.js-required-test').attr('class','js-required-test');
            } else {
                $('.js-country-test').css('display', 'block');
                $('.js-required-test').attr('class','js-required-test cm-required');
            }
        });
    })
}(Tygh, Tygh.$));